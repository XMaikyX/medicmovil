package facci.michaelpenafiel.proyectmedic;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.UUID;

import facci.michaelpenafiel.proyectmedic.model.Cita;

public class DateAddDoctor extends AppCompatActivity {

    Button btn_regresar;

    EditText modU, fecH, houR, pisO, pueR, docT;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_add_doctor);

        fecH=findViewById(R.id.date_txtdoc);
        houR=findViewById(R.id.hour_txtdoc);
        pisO=findViewById(R.id.place_txtdoc);
        pueR=findViewById(R.id.door_txtdoc);
        modU=findViewById(R.id.modulo_doctor);
        docT=findViewById(R.id.doctor_names);

        inicializarFirebase();

        btn_regresar =(Button) findViewById(R.id.btn_regresar);
        btn_regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void inicializarFirebase(){
        FirebaseApp.initializeApp(this);
        firebaseDatabase= FirebaseDatabase.getInstance();
        databaseReference= firebaseDatabase.getReference();
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String fecha= fecH.getText().toString();
        String hora= houR.getText().toString();
        String modulo= modU.getText().toString();
        String piso= pisO.getText().toString();
        String npuerta= pueR.getText().toString();
        String doctor= docT.getText().toString();



        switch (item.getItemId()){
            case R.id.button_add:{
                if (fecha.equals("")|| hora.equals("") || piso.equals("")||npuerta.equals("") || modulo.equals("")|| doctor.equals("")){
                    validation();
                }
                else {
                    Cita c= new Cita();
                    c.setCid(UUID.randomUUID().toString());
                    c.setDoc_name(doctor);
                    c.setModulo(modulo);
                    c.setFecha(fecha);
                    c.setHora(hora);
                    c.setPiso(piso);
                    c.setNpuerta(npuerta);

                    databaseReference.child("Citas").child(c.getCid()).setValue(c);
                    Toast.makeText(this,"Agregada Cita", Toast.LENGTH_LONG).show();
                    limpiarcaja();
                    break;

                }
                break;

            }
            default:break;
        }
        return true;
    }
    private void limpiarcaja() {
        fecH.setText("");
        houR.setText("");
        pisO.setText("");
        pueR.setText("");
    }


    private void validation() {
        String fecha= fecH.getText().toString();
        String hora= houR.getText().toString();
        String piso= pisO.getText().toString();
        String npuerta= pueR.getText().toString();


        if (fecha.equals("")){
            fecH.setError("Required");
        }
        if (hora.equals("")){
            houR.setError("Required");
        }
        if (piso.equals("")){
            pueR.setError("Required");
        }
        if (npuerta.equals("")){
            pisO.setError("Required");
        }
    }
}