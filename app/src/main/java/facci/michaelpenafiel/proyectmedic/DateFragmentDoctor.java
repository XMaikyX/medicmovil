package facci.michaelpenafiel.proyectmedic;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import facci.michaelpenafiel.proyectmedic.model.Cita;
import facci.michaelpenafiel.proyectmedic.model.Doctor;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DateFragmentDoctor#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DateFragmentDoctor extends Fragment {


    Button add_date;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public DateFragmentDoctor() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DateFragmentDoctor.
     */
    // TODO: Rename and change types and number of parameters
    public static DateFragmentDoctor newInstance(String param1, String param2) {
        DateFragmentDoctor fragment = new DateFragmentDoctor();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




        if (getArguments() != null) {

            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_date_doctor, container, false);
        add_date=view.findViewById(R.id.btn_adddate);
        //remove_date=view.findViewById(R.id.btn_d1);


        add_date.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View vu) {
                startActivity(new Intent(getActivity(),listCitas.class));

            }
        });
        /*remove_date.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View vu) {
                startActivity(new Intent(getActivity(),DateEditDoctor.class));

            }
        });*/
        return view;

    }
}