package facci.michaelpenafiel.proyectmedic;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class FragmentAdapterDoctor extends FragmentStateAdapter {
    public FragmentAdapterDoctor (@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position)

        {
            case 1 :
                return new MapFragmentDoctor();
            case 2 :
                return new DateFragmentDoctor();
            case 3 :
                return new UserFragmentDoctor();
        }

        return new HomeFragmentDoctor();

    }

    @Override
    public int getItemCount() { return 4; }
}
