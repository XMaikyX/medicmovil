package facci.michaelpenafiel.proyectmedic;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import facci.michaelpenafiel.proyectmedic.model.Doctor;
import facci.michaelpenafiel.proyectmedic.model.Modulo;

public class AdminApartament extends AppCompatActivity {
    private List<Modulo> listModulo = new ArrayList<Modulo>();
    ArrayAdapter<Modulo> arrayAdapterModulo;
    Button btn_regresar, addmodule;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    ListView listmodulo;
    EditText nomD;

    Modulo moduloSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_apartament);

        nomD =findViewById(R.id.name_texts);
        btn_regresar =(Button) findViewById(R.id.btn_regresar);
        btn_regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        addmodule =(Button) findViewById(R.id.btn_adddate);
        addmodule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent h = new Intent(AdminApartament.this, AdminRegisterModule.class);
                startActivity(h);
            }
        });
        //se LLama el parametro que se creo arriba para asi hacer una referencia al nombre de la tabla
        //en la siguiente linea, esto se realiza para todos los parametros de registro de usuarios
        // añadir una nueva Cita y poder mostar los valores y parametros
        listmodulo=findViewById(R.id.lst_Modulos);
        //Esta Funcion hace que al seleccionar un nombre o un Item, se pueda vizualizar los datos
        listmodulo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                moduloSelected = (Modulo) parent.getItemAtPosition(position);
                nomD.setText(moduloSelected.getNombre());
            }
        });
        //inicializamos firebase y tambien se muestra la lista de los modulos, esto aplica tambien
        //para las citas y modulos
        inicializarFirebase();
        listaModulo();



    }
    //Este metodo Inicializa Firebase para tener una coneccion con Firebase, tambien se añadio
    //la persistencia de datos pero es mas recomendable realizarla en otro archivo java y esta esta
    //en model firebase app y se la añade a Android manifest para referenciarla de manera principal
    private void inicializarFirebase(){
        FirebaseApp.initializeApp(this);
        firebaseDatabase= FirebaseDatabase.getInstance();
        //firebaseDatabase.setPersistenceEnabled(true);
        databaseReference= firebaseDatabase.getReference();
    }
    //En este metodo realizamos lo que es llamar a los parametros de Firebase para mostrarlos en una lista
    //Aqui se utiliza en nombre de Doctores el cual hace referencia al nombre
    private void listaModulo() {
        databaseReference.child("Modulos").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listModulo.clear();
                for (DataSnapshot objSnaptshot : dataSnapshot.getChildren()){
                    Modulo d = objSnaptshot.getValue(Modulo.class);
                    listModulo.add(d);

                    arrayAdapterModulo = new ArrayAdapter<Modulo>(AdminApartament.this, android.R.layout.simple_list_item_1, listModulo);
                    listmodulo.setAdapter(arrayAdapterModulo);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_crud,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String nombre= nomD.getText().toString();

        switch (item.getItemId()){

            case R.id.button_delete:{
                Modulo d =new Modulo();
                d.setDid(moduloSelected.getDid());
                databaseReference.child("Modulos").child(d.getDid()).removeValue();
                Toast.makeText(this,"Eliminado Modulo", Toast.LENGTH_LONG).show();
                limpiarcaja();
                break;
            }
            case R.id.buttom_save:{
                Modulo d = new Modulo();
                d.setDid(moduloSelected.getDid());
                d.setNombre(nomD.getText().toString().trim());
                databaseReference.child("Modulos").child(d.getDid()).setValue(d);
                Toast.makeText(this,"Cambios Realizados", Toast.LENGTH_LONG).show();
                limpiarcaja();
                break;
            }
            default:break;
        }
        return true;
    }
    private void limpiarcaja() {

        nomD.setText("");
    }

}