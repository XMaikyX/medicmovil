package facci.michaelpenafiel.proyectmedic;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.annotations.NotNull;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login_Doctor extends AppCompatActivity {

    Button logi, atopico;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_doctor);

        logi=(Button)findViewById(R.id.btn_logindoctor);
        atopico=(Button)findViewById(R.id.btn_atopicbuton);

        FirebaseMessaging.getInstance().subscribeToTopic("enviaratodos").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<Void> task) {

                Toast.makeText(Login_Doctor.this, "Activado Boton para notificar a cualquiera", Toast.LENGTH_SHORT).show();

            }
        });

        logi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login_Doctor.this, MainActivityDoctor.class);
                startActivity(i);

                llamarEspecifico();
            }
        });
        atopico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llamarAtopico();
            }
        });

    }//oncreate

    private void llamarEspecifico() {
        RequestQueue myrequest= Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();

        try {
            String token = "f7m_OCEoRS6Ev3skQVg9Uu:APA91bEbYMiX9o0NUb8EBoQZrN2igYArTv94A0HOdcsslKmxLbKbrzkwVUUFtHJxhUeuHMn2k2CsOFs1eEdH36LsWcvik4r7edkfSYtWms-P81Q7d5g2SV8F7LEiI-CNrzayPrdIWfPr";

            json.put("to",token);
            JSONObject notificacion = new JSONObject();
            notificacion.put("titulo","Loguin");
            notificacion.put("detalle", "Usuario Doctor se ha Logueado");

            json.put("data", notificacion);

            String URL = "https://fcm.googleapis.com/fcm/send";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,URL,json,null,null){
                @Override
                public Map<String, String> getHeaders(){
                    Map<String,String> header = new HashMap<>();

                    header.put("content-type", "application/json");
                    header.put("authorization", "key=AAAAI0L4u8o:APA91bHfXnaREV86-G8G-ZFXmGsPdGIM5kjols3uhGBV5P7cLeq8xVR5iUMJizkn0y3sYelIDDDf_eVwJ1cpxZG5RsSyuHeD2LeDk847jJNl5qMkli1WFIAOvse9XbDad0VUlvQrFR8O");

                    return  header;

                }
            };

            myrequest.add(request);

        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void llamarAtopico() {
        RequestQueue myrequest= Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();

        try {
            //String token = "ddHfsxvAQ0e6UviCkrebzR:APA91bE-qNPCZu5qJ7eXatPoovncdK95hUV7_S625TJUrVGbpAtWXEhpXDyPRtXk94zaI3luwQsR2MP5ectvAPV7a7QY9TFf41POLt4DJVa3IBImo7asbCWjWMxMAKknHmTxaEhN9qnQ";

            json.put("to","/topics/"+"enviaratodos");
            JSONObject notificacion = new JSONObject();
            notificacion.put("titulo","Loguin");
            notificacion.put("detalle", "Usuario Doctor se ha Logueado");

            json.put("data", notificacion);

            String URL = "https://fcm.googleapis.com/fcm/send";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,URL,json,null,null){
                @Override
                public Map<String, String> getHeaders(){
                    Map<String,String> header = new HashMap<>();

                    header.put("content-type", "application/json");
                    header.put("authorization", "key=AAAAI0L4u8o:APA91bHfXnaREV86-G8G-ZFXmGsPdGIM5kjols3uhGBV5P7cLeq8xVR5iUMJizkn0y3sYelIDDDf_eVwJ1cpxZG5RsSyuHeD2LeDk847jJNl5qMkli1WFIAOvse9XbDad0VUlvQrFR8O");

                    return  header;

                }
            };

            myrequest.add(request);

        }catch (JSONException e){
            e.printStackTrace();
        }
    }

}