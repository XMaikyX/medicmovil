package facci.michaelpenafiel.proyectmedic.model;

public class Paciente {
    private  String name;
    private  String lastname;
    private  String mail;
    private  String pass;
    public Paciente(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
//Crud: --> login & register de los data del paciente con Firebase
//(Authentication & Realtime Database)...
