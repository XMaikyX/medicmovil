package facci.michaelpenafiel.proyectmedic.model;

public class Modulo {

    private String did;
    private String Nombre;

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre= nombre;
    }
    @Override
    public String toString() {
        return Nombre;
    }
}
//Crud: register --> modify & delete del data Modulo (Especialidad Medicas)
// con Firebase...
