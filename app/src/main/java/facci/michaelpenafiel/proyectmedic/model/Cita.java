package facci.michaelpenafiel.proyectmedic.model;

public class Cita {

    private String cid;
    private String doc_name;
    private String Modulo;
    private String Fecha;
    private String Hora;
    private String Piso;
    private String Npuerta;

    public String getDoc_name() {
        return doc_name;
    }

    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getModulo() {
        return Modulo;
    }

    public void setModulo(String modulo) {
        Modulo = modulo;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }

    public String getHora() {
        return Hora;
    }

    public void setHora(String hora) {
        Hora = hora;
    }

    public String getPiso() {
        return Piso;
    }

    public void setPiso(String piso) {
        Piso = piso;
    }

    public String getNpuerta() {
        return Npuerta;
    }

    public void setNpuerta(String npuerta) {
        Npuerta = npuerta;
    }


    @Override
    public String toString() {
        return Hora +"                                     "+ Npuerta  + "                  " + doc_name;
    }
}
