package facci.michaelpenafiel.proyectmedic.model;

public class Doctor {

    private String did;
    private String Nombre;
    private String Apellido;
    private String Email;
    private String Contrasenia;
    private String Modulo;

    public String getModulo() {
        return Modulo;
    }

    public void setModulo(String modulo) {
        Modulo = modulo;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) { Nombre = nombre; }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getContrasenia() {
        return Contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        Contrasenia = contrasenia;
    }

    @Override
    public String toString() {
        return Nombre +" "+Apellido;
    }
}
