package facci.michaelpenafiel.proyectmedic;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import facci.michaelpenafiel.proyectmedic.model.Cita;
import facci.michaelpenafiel.proyectmedic.model.Doctor;

public class listCitas extends AppCompatActivity {

    private List<Cita> listCita = new ArrayList<Cita>();
    ArrayAdapter<Cita> arrayAdapterCita;

    Button btn_regresar, adddoctor;
    ListView listcita;
    EditText modU, fecH, houR, pisO, pueR, docT;


    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    Cita citaSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_citas);

        fecH=findViewById(R.id.date_txtdocs);
        houR=findViewById(R.id.hour_txtdocs);
        pisO=findViewById(R.id.place_txtdocs);
        pueR=findViewById(R.id.door_txtdocs);
        modU=findViewById(R.id.modulo_doctors);
        docT=findViewById(R.id.doctor_namess);

        //se LLama el parametro que se creo arriba para asi hacer una referencia al nombre de la tabla
        //en la siguiente linea, esto se realiza para todos los parametros de registro de usuarios
        // añadir una nueva Cita y poder mostar los valores y parametros
        listcita=findViewById(R.id.lst_Citas);

        //inicializamod firebase y tambien se muestra la lista de los doctores, esto aplica tambien
        //para las citas y modulos
        inicializarFirebase();
        listaCita();

        //Esta Funcion hace que al seleccionar un nombre o un Item, se pueda vizualizar los datos
        listcita.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                citaSelected = (Cita) parent.getItemAtPosition(position);
                fecH.setText(citaSelected.getFecha());
                houR.setText(citaSelected.getHora());
                pisO.setText(citaSelected.getPiso());
                pueR.setText(citaSelected.getNpuerta());
                modU.setText(citaSelected.getModulo());
                docT.setText(citaSelected.getDoc_name());

            }
        });
        btn_regresar =(Button) findViewById(R.id.btn_regresar);
        btn_regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        adddoctor =(Button) findViewById(R.id.btn_adddates);
        adddoctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent h = new Intent(listCitas.this, DateAddDoctor.class);
                startActivity(h);
            }
        });


    }
    //En este metodo realizamos lo que es llamar a los parametros de Firebase para mostrarlos en una lista
    //Aqui se utiliza en nombre de Doctores el cual hace referencia al nombre
    private void listaCita() {
        databaseReference.child("Citas").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listCita.clear();
                for (DataSnapshot objSnaptshot : dataSnapshot.getChildren()){
                    Cita c = objSnaptshot.getValue(Cita.class);
                    listCita.add(c);

                    arrayAdapterCita = new ArrayAdapter<Cita>(listCitas.this, android.R.layout.simple_list_item_1,listCita);
                    listcita.setAdapter(arrayAdapterCita);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    //Este metodo Inicializa Firebase para tener una coneccion con Firebase, tambien se añadio
    //la persistencia de datos pero es mas recomendable realizarla en otro archivo java y esta esta
    //en model firebase app y se la añade a Android manifest para referenciarla de manera principal
    private void inicializarFirebase(){
        FirebaseApp.initializeApp(this);
        firebaseDatabase= FirebaseDatabase.getInstance();
        //firebaseDatabase.setPersistenceEnabled(true);
        databaseReference= firebaseDatabase.getReference();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_crud,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String fecha= fecH.getText().toString();
        String hora= houR.getText().toString();
        String piso= pisO.getText().toString();
        String npuerta= pueR.getText().toString();
        String doctor= docT.getText().toString();
        String module= modU.getText().toString();


        switch (item.getItemId()){

            case R.id.button_delete:{
                Cita c =new Cita();
                c.setCid(citaSelected.getCid());
                databaseReference.child("Citas").child(c.getCid()).removeValue();
                Toast.makeText(this,"Eliminada Cita", Toast.LENGTH_LONG).show();
                limpiarcaja();
                break;
            }
            case R.id.buttom_save:{
                Cita c = new Cita();
                c.setCid(citaSelected.getCid());
                c.setFecha(fecH.getText().toString().trim());
                c.setHora(houR.getText().toString().trim());
                c.setPiso(pisO.getText().toString().trim());
                c.setNpuerta(pueR.getText().toString().trim());
                c.setModulo(modU.getText().toString().trim());
                c.setDoc_name(docT.getText().toString().trim());



                databaseReference.child("Citas").child(c.getCid()).setValue(c);
                Toast.makeText(this,"Cambios Realizados", Toast.LENGTH_LONG).show();
                limpiarcaja();
                break;
            }
            default:break;
        }
        return true;
    }
    private void limpiarcaja() {
        fecH.setText("");
        houR.setText("");
        pisO.setText("");
        pueR.setText("");
    }


}