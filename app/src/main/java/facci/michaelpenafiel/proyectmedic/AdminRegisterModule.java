package facci.michaelpenafiel.proyectmedic;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.UUID;

import facci.michaelpenafiel.proyectmedic.model.Doctor;
import facci.michaelpenafiel.proyectmedic.model.Modulo;

public class AdminRegisterModule extends AppCompatActivity {

    Button btn_regresar;
    EditText nomB;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_register_module);
        nomB = findViewById(R.id.ls_nametext);
        inicializarFirebase();

        btn_regresar =(Button) findViewById(R.id.btn_regresar);
        btn_regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
    private void inicializarFirebase(){
        FirebaseApp.initializeApp(this);
        firebaseDatabase= FirebaseDatabase.getInstance();
        databaseReference= firebaseDatabase.getReference();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String nombre= nomB.getText().toString();

        switch (item.getItemId()){
            case R.id.button_add:{
                if (nombre.equals("")){
                    validation();
                }
                else {
                    Modulo m= new Modulo();
                    m.setDid(UUID.randomUUID().toString());
                    m.setNombre(nombre);

                    databaseReference.child("Modulos").child(m.getDid()).setValue(m);
                    Toast.makeText(this,"Agregado Modulo", Toast.LENGTH_LONG).show();
                    limpiarcaja();
                    break;

                }
                break;

            }
            default:break;
        }
        return true;
    }
    private void limpiarcaja()
    {
        nomB.setText("");
    }


    private void validation() {
        String nombre=nomB.getText().toString();


        if (nombre.equals("")){
            nomB.setError("Required");
        }
    }
}