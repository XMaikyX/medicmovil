package facci.michaelpenafiel.proyectmedic;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ComentaryDoctor extends AppCompatActivity {

    Button btn_regresar, btn_select;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comentary_doctor);

        btn_regresar =(Button) findViewById(R.id.btn_regresar);
        btn_select=(Button) findViewById((R.id.btn_user1)) ;



        btn_regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent h = new Intent(ComentaryDoctor.this, CheckComentaryDoctor.class);
                startActivity(h);
            }
        });
    }
}