package facci.michaelpenafiel.proyectmedic;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.UUID;

import facci.michaelpenafiel.proyectmedic.model.Doctor;

public class AdminRegisterDoctor extends AppCompatActivity {

    Button btn_regresar;
    EditText nomB, apeL,correO,pasS, module;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_register_doctor);

        //LLamar a las variables para seleccionar el nombre del espacio a seleccionar
        nomB=findViewById(R.id.ls_nametext);
        apeL=findViewById(R.id.lastname_text);
        module=findViewById(R.id.module_text);
        correO=findViewById(R.id.email_text);
        pasS=findViewById(R.id.password_text);
        inicializarFirebase();

        /*listdoctor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                doctorSelected =(Doctor) parent.getItemAtPosition(position);
                nomB.setText(doctorSelected.getNombre());
                apeL.setText(doctorSelected.getApellido());
                module.setText(doctorSelected.getModulo());
                correO.setText(doctorSelected.getEmail());
                pasS.setText(doctorSelected.getContrasenia());
            }
        });*/

        btn_regresar =(Button) findViewById(R.id.btn_regresar);
        btn_regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    private void inicializarFirebase(){
        FirebaseApp.initializeApp(this);
        firebaseDatabase= FirebaseDatabase.getInstance();
        databaseReference= firebaseDatabase.getReference();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String nombre= nomB.getText().toString();
        String apellido= apeL.getText().toString();
        String modulo= module.getText().toString();
        String email= correO.getText().toString();
        String contrasenia= pasS.getText().toString();


        switch (item.getItemId()){
            case R.id.button_add:{
                if (nombre.equals("")|| apellido.equals("") || email.equals("")||contrasenia.equals("") || modulo.equals("")){
                    validation();
                }
                else {
                    Doctor d= new Doctor();
                    d.setDid(UUID.randomUUID().toString());
                    d.setNombre(nombre);
                    d.setModulo(modulo);
                    d.setApellido(apellido);
                    d.setEmail(email);
                    d.setContrasenia(contrasenia);

                    databaseReference.child("Doctores").child(d.getDid()).setValue(d);
                    Toast.makeText(this,"Agregado Doctor", Toast.LENGTH_LONG).show();
                    limpiarcaja();
                    break;

                }
                break;

            }
            default:break;
        }
        return true;
    }

    private void limpiarcaja() {
        nomB.setText("");
        apeL.setText("");
        module.setText("");
        correO.setText("");
        pasS.setText("");
    }


    private void validation() {
        String nombre= nomB.getText().toString();
        String apellido= apeL.getText().toString();
        String modulo= module.getText().toString();
        String correo= correO.getText().toString();
        String contrasenia= pasS.getText().toString();


        if (nombre.equals("")){
            nomB.setError("Required");
        }
        if (apellido.equals("")){
            apeL.setError("Required");
        }
        if (modulo.equals("")){
            module.setError("Required");
        }
        if (correo.equals("")){
            correO.setError("Required");
        }
        if (contrasenia.equals("")){
            pasS.setError("Required");
        }
    }
}