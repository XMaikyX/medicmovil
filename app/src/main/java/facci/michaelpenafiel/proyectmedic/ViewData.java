package facci.michaelpenafiel.proyectmedic;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import facci.michaelpenafiel.proyectmedic.model.Cita;

public class ViewData extends AppCompatActivity {

    private List<Cita> listCita = new ArrayList<Cita>();
    ArrayAdapter<Cita> arrayAdapterCita;

    Button btn_regresar, btn_example;
    ListView listcita;


    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_data);


        //se LLama el parametro que se creo arriba para asi hacer una referencia al nombre de la tabla
        //en la siguiente linea, esto se realiza para todos los parametros de registro de usuarios
        // añadir una nueva Cita y poder mostar los valores y parametros
        listcita=findViewById(R.id.lst_Citass);

        //inicializamod firebase y tambien se muestra la lista de los doctores, esto aplica tambien
        //para las citas y modulos
        inicializarFirebase();
        listaCita();

        //Esta Funcion hace que al seleccionar un nombre o un Item, se pueda vizualizar los datos

        /*btn_example=(Button)findViewById(R.id.btn_h1);

        btn_example.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent h = new Intent(ViewData.this, DatedAdd.class);
                startActivity(h);
            }
        });*/

        btn_regresar =(Button) findViewById(R.id.btn_regresar);
        btn_regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }


        });

    }
    //En este metodo realizamos lo que es llamar a los parametros de Firebase para mostrarlos en una lista
    //Aqui se utiliza en nombre de Doctores el cual hace referencia al nombre
    private void listaCita() {
        databaseReference.child("Citas").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listCita.clear();
                for (DataSnapshot objSnaptshot : dataSnapshot.getChildren()){
                    Cita c = objSnaptshot.getValue(Cita.class);
                    listCita.add(c);

                    arrayAdapterCita = new ArrayAdapter<Cita>(ViewData.this, android.R.layout.simple_list_item_1,listCita);
                    listcita.setAdapter(arrayAdapterCita);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    //Este metodo Inicializa Firebase para tener una coneccion con Firebase, tambien se añadio
    //la persistencia de datos pero es mas recomendable realizarla en otro archivo java y esta esta
    //en model firebase app y se la añade a Android manifest para referenciarla de manera principal
    private void inicializarFirebase(){
        FirebaseApp.initializeApp(this);
        firebaseDatabase= FirebaseDatabase.getInstance();
        //firebaseDatabase.setPersistenceEnabled(true);
        databaseReference= firebaseDatabase.getReference();
    }

}