package facci.michaelpenafiel.proyectmedic;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import facci.michaelpenafiel.proyectmedic.model.Doctor;

public class AdminDoctor extends AppCompatActivity {

    private List<Doctor> listDoctor = new ArrayList<Doctor>();
    ArrayAdapter<Doctor> arrayAdapterDoctor;

    Button btn_regresar, adddoctor;
    ListView listdoctor;
    EditText nomD, apeL,correO,pasS, module;


    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    Doctor doctorSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_doctor);

        nomD =findViewById(R.id.name_texts);
        apeL=findViewById(R.id.lastname_texts);
        correO=findViewById(R.id.email_texts);
        module=findViewById(R.id.module_texts);
        pasS=findViewById(R.id.password_texts);


        //se LLama el parametro que se creo arriba para asi hacer una referencia al nombre de la tabla
        //en la siguiente linea, esto se realiza para todos los parametros de registro de usuarios
        // añadir una nueva Cita y poder mostar los valores y parametros
        listdoctor=findViewById(R.id.lst_Doctores);


        //inicializamos firebase y tambien se muestra la lista de los doctores, esto aplica tambien
        //para las citas y modulos
        inicializarFirebase();
        listaDoctor();

        //Esta Funcion hace que al seleccionar un nombre o un Item, se pueda vizualizar los datos
        listdoctor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                doctorSelected = (Doctor) parent.getItemAtPosition(position);
                nomD.setText(doctorSelected.getNombre());
                apeL.setText(doctorSelected.getApellido());
                module.setText(doctorSelected.getModulo());
                correO.setText(doctorSelected.getEmail());
                pasS.setText(doctorSelected.getContrasenia());
            }
        });

        btn_regresar =(Button) findViewById(R.id.btn_regresar);
        btn_regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        adddoctor =(Button) findViewById(R.id.btn_adddate);
        adddoctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent h = new Intent(AdminDoctor.this, AdminRegisterDoctor.class);
                startActivity(h);
            }
        });
    }

    //En este metodo realizamos lo que es llamar a los parametros de Firebase para mostrarlos en una lista
    //Aqui se utiliza en nombre de Doctores el cual hace referencia al nombre
    private void listaDoctor() {
        databaseReference.child("Doctores").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listDoctor.clear();
                for (DataSnapshot objSnaptshot : dataSnapshot.getChildren()){
                    Doctor d = objSnaptshot.getValue(Doctor.class);
                    listDoctor.add(d);

                    arrayAdapterDoctor = new ArrayAdapter<Doctor>(AdminDoctor.this, android.R.layout.simple_list_item_1,listDoctor);
                    listdoctor.setAdapter(arrayAdapterDoctor);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    //Este metodo Inicializa Firebase para tener una coneccion con Firebase, tambien se añadio
    //la persistencia de datos pero es mas recomendable realizarla en otro archivo java y esta esta
    //en model firebase app y se la añade a Android manifest para referenciarla de manera principal
    private void inicializarFirebase(){
        FirebaseApp.initializeApp(this);
        firebaseDatabase= FirebaseDatabase.getInstance();
        //firebaseDatabase.setPersistenceEnabled(true);
        databaseReference= firebaseDatabase.getReference();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_crud,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String nombre= nomD.getText().toString();
        String apellido= apeL.getText().toString();
        String modulo= module.getText().toString();
        String email= correO.getText().toString();
        String contrasenia= pasS.getText().toString();


        switch (item.getItemId()){

            case R.id.button_delete:{
                Doctor d =new Doctor();
                d.setDid(doctorSelected.getDid());
                databaseReference.child("Doctores").child(d.getDid()).removeValue();
                Toast.makeText(this,"Eliminado Doctor", Toast.LENGTH_LONG).show();
                limpiarcaja();
                break;
            }
            case R.id.buttom_save:{
                Doctor d = new Doctor();
                d.setDid(doctorSelected.getDid());
                d.setNombre(nomD.getText().toString().trim());
                d.setApellido(apeL.getText().toString().trim());
                d.setEmail(correO.getText().toString().trim());
                d.setModulo(module.getText().toString().trim());
                d.setContrasenia(pasS.getText().toString().trim());
                databaseReference.child("Doctores").child(d.getDid()).setValue(d);
                Toast.makeText(this,"Cambios Realizados", Toast.LENGTH_LONG).show();
                limpiarcaja();
                break;
            }
            default:break;
        }
        return true;
    }
    private void limpiarcaja() {
        nomD.setText("");
        apeL.setText("");
        module.setText("");
        correO.setText("");
        pasS.setText("");
    }
}