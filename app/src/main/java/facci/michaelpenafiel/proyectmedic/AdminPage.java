package facci.michaelpenafiel.proyectmedic;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class AdminPage extends AppCompatActivity {

    Button btnapart, btndoctor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_page);

        btnapart =(Button) findViewById(R.id.btn_apartament);
        btnapart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent h = new Intent(AdminPage.this, AdminApartament.class);
                startActivity(h);
            }
        });
        btndoctor =(Button) findViewById(R.id.btn_doctor);
        btndoctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent h = new Intent(AdminPage.this, AdminDoctor.class);
                startActivity(h);
            }
        });
    }
}